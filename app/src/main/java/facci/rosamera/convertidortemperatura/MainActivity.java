package facci.rosamera.convertidortemperatura;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {

    EditText temp;
    RadioButton toC;
    RadioButton toF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        temp = (EditText)findViewById(R.id.temperaturaEditText);
        toC = (RadioButton)findViewById(R.id.paraCelciosRadioButton2);
        toF = (RadioButton)findViewById(R.id.paraFarenheitRadioButton);
    }

    public void convertir(View v){
        double value = new Double(temp.getText().toString());
        if(toC.isChecked())
            value = SoloConversion.FarenheitParacelcios(value);
        else
            value = SoloConversion.celciosParaFarenheit(value);
        temp.setText(new Double(value).toString());
    }
}
